﻿Imports System.IO

Public Class Form1
    Dim wc As System.Net.WebClient
    Private Async Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim path As String
        path = My.Application.Info.DirectoryPath
        Dim lanzador As String
        lanzador = path + "\EsusLauncherA3.exe"
        If File.Exists(lanzador) Then
            My.Computer.FileSystem.DeleteFile(lanzador)
        End If
        wc = New System.Net.WebClient()
        AddHandler wc.DownloadProgressChanged, AddressOf OnDownloadProgressChanged
        AddHandler wc.DownloadFileCompleted, AddressOf OnFileDownloadCompleted
        wc.DownloadFileAsync(New Uri("https://www.dropbox.com/s/sge85zk1zz52cpv/EsusLauncherA3.exe?raw=1"), lanzador)
    End Sub
    Private Sub OnDownloadProgressChanged(ByVal sender As Object, ByVal e As System.Net.DownloadProgressChangedEventArgs)
        Dim totalSize As Long = e.TotalBytesToReceive
        Dim downloadedBytes As Long = e.BytesReceived
        Dim percentage As Integer = e.ProgressPercentage
        ProgressBar1.Value = percentage
        Label2.Text = percentage
        'Put your progress UI here, you can cancel download by uncommenting the line below
        'wc.CancelAsync()
    End Sub
    Private Sub OnFileDownloadCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.AsyncCompletedEventArgs)
        If e.Cancelled Then
        ElseIf Not e.Error Is Nothing Then
        Else
            Dim path As String
            path = My.Application.Info.DirectoryPath
            Dim lanzador As String
            lanzador = path + "\EsusLauncherA3.exe"
            Try
                Process.Start(lanzador)
            Catch ex As Exception

            End Try
            'File Downloaded Successfuly
            Application.Exit()
        End If
    End Sub

End Class
